# README #

Hello Everybody, 

We have a test task that we would like to give you to assess your suitability for this project.

For a weekly class schedule, we need a dropdown that allows you to navigate between weeks (select a new week in a monthly view). The design for the week picker can be found in the following link.

https://www.dropbox.com/s/clhj7jw8m9xo5kq/Week%20Picker.psd?dl=0

Here you can find the code for the Weekly Schedule.

To see the Weekly Schedule, click on the clipboard button:

https://www.dropbox.com/s/myq42goi7co4i5y/Screenshot%202015-03-18%2010.33.46.png?dl=0

Then scroll down to the bottom of the page. The classes, if you care to see them, are only viewable for the week of January 5th. 

We don't expect you to complete this before we hire you but we need to see that you understand and can handle work of this nature. We would like an estimate on how many hours you believe it would take you to complete this task, when you believe you could have it done and how you would handle it. 

Of course, contact me if you have any questions or if you would like to know more about who we are and what we are doing.

All the best,

-Triglav Team

#How to do

1 - Do a clone of this project

2 - Do your changes

3 - Send an e-mail for kleberpinel@triglavllc.com with your repository URL with your changes

4 - Nothing more to do, just wait our contact.

5 - Thanks